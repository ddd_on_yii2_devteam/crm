<?php
$params = array_merge(require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'), require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php'));

$routes = require(__DIR__ . '/../../api/config/routes.php');

return [
    'id'                  => 'app-api',
    'basePath'            => dirname(__DIR__),
    'bootstrap'           => ['log'],
    'controllerNamespace' => 'api\controllers',
    'components'          => [
        'user'         => [
            'identityClass'   => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log'          => [
            'traceLevel' => YII_DEBUG ? 0 : 0,
            'targets'    => [
                [
                    'class'   => 'yii\log\FileTarget',
                    'levels'  => ['error', 'warning'],
                    'logVars' => [],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'request'      => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
        ],
        'urlManager'   => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,
            'rules'           => array_merge([
                [
                    //Получение переводов
                    'class'         => 'yii\rest\UrlRule',
                    'controller'    => [
                        'seller',
                        'manufacturer',
                    ],
                    'tokens'        => [
                        '{id}' => '<id:\\d[\\d,]*>',
                    ],
                    'extraPatterns' => [
                        'GET {id}/translation' => 'getTranslationData',
                    ],
                    'pluralize'     => false,

                ],
            ], $routes),
        ],
        'response'     => [
            'format'  => yii\web\Response::FORMAT_JSON,
            'charset' => 'UTF-8',
        ],
    ],

    'params'  => $params,
    'modules' => [
        'billing' => [
            'class' => \api\modules\billing\Module::class,
        ],
    ],
];
