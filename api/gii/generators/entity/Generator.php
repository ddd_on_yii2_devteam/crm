<?php

namespace api\gii\generators\entity;

use commonprj\components\core\entities\element\Element;
use commonprj\components\core\models\ElementClassRecord;
use Yii;
use yii\db\Query;
use yii\gii\CodeFile;

/**
 * This generator will generate one or multiple ActiveRecord classes for the specified database table.
 *
 * add 'generators' to main-local.php
 *
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => [],
        'generators' => [
            'entity' => ['class' => api\gii\generators\entity\Generator::class]
        ],
    ];
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class Generator extends \yii\gii\Generator
{

    public $ns;
    public $controllerNs;
    public $db = 'db';
    public $baseClass = Element::class;
    public $entityContext;
    public $entityName;
    public $generateAll;

    private $allowedContexts = [
        'catalog',
        'crm',
        'tcm',
    ];

    /**
     * @inheritdoc
     */
    public function getName()
    {
        return 'Entity Generator';
    }

    /**
     * @inheritdoc
     */
    public function getDescription()
    {
        return 'This generator generates an Entity';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return array_merge(parent::rules(), [
            ['entityContext', 'string'],
            ['entityName', 'string'],
            ['generateAll', 'boolean'],
        ]);
    }

    /**
     * @inheritdoc
     */
    public function generate()
    {
        $files = [];

        if ($this->generateAll) {
            $elementClasses = ElementClassRecord::find()->all();

            foreach (($elementClasses) as $elementClass) {
                list($context, $entity) = @explode('\\', $elementClass->name);
                if ($context && $entity && in_array($context, $this->allowedContexts)) {
                    $this->entityContext = $context;
                    $this->entityName = $entity;
                    $files = array_merge($files, $this->renderEntity());
                }
            }

        } else {
            if ($this->entityContext && $this->entityName) {
                $files = $this->renderEntity();
            }
        }

        return $files;
    }

    private function renderEntity()
    {
        $files = [];
        $this->ns = sprintf('commonprj\components\%s\entities\%s',
            lcfirst($this->entityContext),
            lcfirst($this->entityName)
        );

        $this->controllerNs = sprintf('api\modules\%s\controllers',
            lcfirst($this->entityContext)
        );

        $entityFile = sprintf('%s/components/%s/entities/%s/%s.php',
            Yii::getAlias('@commonprj'),
            lcfirst($this->entityContext),
            lcfirst($this->entityName),
            ucfirst($this->entityName)
        );

        $repositoryFile = sprintf('%s/components/%s/entities/%s/%sDBRepository.php',
            Yii::getAlias('@commonprj'),
            lcfirst($this->entityContext),
            lcfirst($this->entityName),
            ucfirst($this->entityName)
        );

        $controllerFile = sprintf('%s/modules/%s/controllers/%sController.php',
            Yii::getAlias('@api'),
            lcfirst($this->entityContext),
            ucfirst($this->entityName)
        );

        $files[] = new CodeFile($entityFile, $this->render('entity.php'));
        $files[] = new CodeFile($repositoryFile, $this->render('repository.php'));
        $files[] = new CodeFile($controllerFile, $this->render('controller.php'));

        return $files;
    }

}
