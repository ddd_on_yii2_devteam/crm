<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\crm\entities\permissionGroup\PermissionGroup;

/**
 * Class PermissionGroupController
 * @package api\controllers
 */
class PermissionGroupController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = PermissionGroup::class;

}