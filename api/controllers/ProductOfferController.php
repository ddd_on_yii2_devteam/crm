<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\crm\entities\productOffer\ProductOffer;

/**
 * Class ProductOfferController
 * @package api\controllers
 */
class ProductOfferController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = ProductOffer::class;

}