<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\crm\entities\address\Address;

/**
 * Class AddressController
 * @package api\controllers
 */
class AddressController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = Address::class;

}