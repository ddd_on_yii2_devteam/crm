<?php

namespace api\controllers;

use common\extendedStdComponents\CommonElementActiveController;
use commonprj\components\crm\entities\permission\Permission;

/**
 * Class PermissionController
 * @package api\controllers
 */
class PermissionController extends CommonElementActiveController
{
    /**
     * @var string
     */
    public $modelClass = Permission::class;

}